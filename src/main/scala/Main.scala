import java.nio.file.{Files, Path, Paths, StandardOpenOption}
import java.security.MessageDigest

import cats.effect.IO
import scodec.bits.ByteVector
import scodec.stream.{StreamDecoder, decode => D}
import scodec.{codecs => C}

import scala.language.postfixOps

object Main {
  final case class Params(pkFilePath: Path, outputPath: Path)

  def main(args: Array[String]): Unit = {
    parseArgs(args) match {
      case Right(params) =>
        val files = pakDecoder
          .decodeInputStream[IO](Files.newInputStream(params.pkFilePath), 4096)
          .evalMap(writeFile(params.outputPath))
          .compile
          .toList
          .unsafeRunSync()
        println(s"Written ${files.size} OGG files to ${params.outputPath}")
      case Left(err) => println(err)
    }
  }

  def parseArgs(args: Seq[String]): Either[String, Params] = args match {
    case Seq(pkFile, output) =>
      Right(Params(Paths.get(pkFile), Paths.get(output)))
    case _ =>
      Left("Expecting 2 parameters: path to a PK file and an output directory")
  }

  val pakDecoder: StreamDecoder[Array[Byte]] = for {
    headerSize <- D.once(C.int32L)
    _ <- D.drop(headerSize * 8)
    chunk <- D.many(C.variableSizeBytesLong(C.int64L, C.bytes))
  } yield decodeChunk(chunk)

  def decodeChunk(vec: ByteVector): Array[Byte] = {
    val bytes = vec.toArray
    if (!vec.startsWith(ByteVector("OGG".getBytes)))
      bytes.indices.foreach(i => bytes(i) = bytes(i) - 1 toByte)
    bytes
  }

  def writeFile(outputPath: Path)(bytes: Array[Byte]): IO[Path] = IO {
    val digest = MessageDigest.getInstance("MD5").digest(bytes)
    val filePath = Paths.get(outputPath.toString, digest.map("%02X" format _).mkString + ".ogg")
    Files.write(filePath, bytes)
  }
}
