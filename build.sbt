name := "paganini"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  "org.scodec" %% "scodec-stream" % "1.1.0",
  "org.scodec" %% "scodec-bits" % "1.1.5"
)
