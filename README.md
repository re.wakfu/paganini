This is a small tool that can extract PK sound archives

# Usage
```
sbt "run /path/to/musics.pk /path/to/output/directory"
```
The above will unpack all sound files in OGG format to the specified directory.
Note: output directory has to be an existing one
